**417 Dominion – an asynchronous Necromunda Dominion campain**

This campaign is a modification of the Dominion Campaign of the general Necromunda Rulebook of 2018, p78. The main difference is, that territories are not chosen by participants but allocated by chance (overall one time per campaign phase). This way participatns can meet asynchronously (not all together on a certain day but just the matched players for their specific matches on different days and in smaller, more flexible groups).

# General information

## Abbreviations

| Abbreviation       | Meaning |
| ---                | ---     |
| OoA                | Out of Action |
| p<123>             | Page number in general Necromunda Rulebook of 2018 |
| Pre-BS             | Pre-Battle Sequence |
| Post-BS            | Post-Battle Sequence |

## Campaign schedule

- Campaign duration: 6 months
    - start: Q4 2022
    - end: Q1 2023.
- Duration of a campaign phase: 3 weeks

| Phase | First phase day | Last phase day  | Note |
| ---   | ---             | ---             | ---  |
| 1     | 2022-09-26 (Mo) | 2022-10-23 (Su) | extended by 1w |
| 2     | 2022-10-24 (Mo) | 2022-11-13 (Su) |      |

## Participating Gangs

Ordered by registration day:

| Player ID | Matrix name      | Faction             | Gang name                     | Gang ID | Affiliation | Participated in phase |
| ---       | ---              | ---                 | ---                           | ---     | ---         | ---                   |
| 1         | lunokhod         | Clanless Outcasts   | Amber's Bots                  | AmBot   | neither     | 1                     |
| 2         | nyamtsoi         | Genestealer Cults   | Devoted of the Star Brethren  | DSB     | Outlaw      |                       |
| 3         | the_void         | Enforcers           | HLI Morgwan's req. prec. 14¹  | P14     | Law-Abiding | 1                     |
| 4         | mrrobat          | House Goliath       | MILK Raiders                  | MILK    | Law-Abiding |                       |
| 5         | ducyboy          | House Escher        | No Prayer for the Dying       | NPFTD   | Law-Abiding |                       |
| 6         | fry2nice         | House Cawdor        | purity fighters               | pf      | Outlaw      | 1                     |
| 7         | ﻿bountainmike0815 | House Escher        | Nachthexen                    | nh      | Law-Abiding |                       |


¹"His Lord Inquisitor Morgwan's requisitioned precinct 14"

Because some factions didn't exist when the Dominion campaign was developed (in 2018), some of them count as the following houses for Territory Boons:
* Enforcers: House Goliath
* Genestealer Cults: House Orlock
* Clanless Outcasts: House Orlock

## Important links

- [417 Dominion Campaign Reference](https://gitlab.com/r4dh4l/417dominion/-/blob/main/campaign_reference.pdf)
- Printing templates recommended by the Campaign Arbitrator:
    - [runCFC Fighter Cards](https://gitlab.com/r4dh4l/runcfc)
    - [runCGR Gang Roster](https://gitlab.com/r4dh4l/runcgr)
        - [runCGR Cashflow Sheet](https://gitlab.com/r4dh4l/runcgr/-/blob/main/runcgr_cashflow.pdf)

## Campaign rules

- Dominion campaign, modified for asynchronous mode (Territory Fights are matched ba chance)
- starting Gang size is `500 credits`
- all official GW factions allowed
- campaign communication via [[matrix](https://element.io/)]
- Campaign duration: begin of `Q4 2022` until end of `Q1 2023` (6 months)
    - one campaign phase lasts 3 weeks
- Matches per campaign phase and participant:
    - main plot: one Territory Fight
    - side plot: as many Non-Territory Fights as a participant like (with certain limits provided by the specific scenario)
- Strict WYSIWYG is not forced, but a close combat Fighter with a huge weapon should not be represented by a close combat Fighter with a light weapon, for example.
- Conversions are allowed (playing Sisters of Battle modified for Necromunda in style as a whole House Cawdor gang, for example), proxies not (if you don't have a model to represent a convincing Rogue Doc model you can not use a model of a Space Marine to represent it). Criteria for this is a convincing effect of "immersion".
- Asynchronous mode (no general group dates, participants meet self organized whenever they are matched for a Territory Fight per campaign phase)

For **FAQ and Errata** see section "FAQ and Errata" at the end of this document.

## What to prepare

### When joining the campaign

1. Get the following documents:
    1. Fighter Cards for your gang fighters. The Arbitrator recommends to use [ruNCFC](https://gitlab.com/r4dh4l/runcfc) instead of the official GW Fighter Cards.
    2. A Gang Roster for your gang. The Arbitrator recommends to use [ruNCGR](https://gitlab.com/r4dh4l/runcgr) instead of the official GW Gang Roster.
    3. The [417 Dominion Campaign Reference](https://gitlab.com/r4dh4l/417dominion/-/blob/main/campaign_reference.pdf) for you and just for the case a second time for your opponent. Although under permanent development the Arbitrator recommends to print it before playing a match (it will be a "showstopper" if you read it online and run out of energy during a match).
    4. From [Warhammer Community Download section](https://www.warhammer-community.com/downloads/#necromunda):
        1. **"The Trading Post"** (contains almost all items and all weapon traits)
        2. **"Tokens"** (just if you don't already have Tokens from a starter box, of course)
        3. **"Necromunda Web Exclusive Tactics Cards"** if you don't have own Tactics Cards or, if you like, in addition to your existing Tactics Cards.
     5. The Necromunda Rulebook of 2018 (the Campaign Rules and Reference will refer to chapters in this book sometimes).
     6. From the "Book of Judgement" (2019) the chapter "The Black Market Trading Post" (p93-127).
2. Create a Gang according to the "Campaign gaming rules" section above.
    * Optional: To prevent controversies about the exact front of a model, paint a small dot on the front of the base indicating the middle of the fighters field of vision.
3. Prepare the following documents:
    1. A [fighter card](https://gitlab.com/r4dh4l/runcfc) for any of your gang fighters.
    2. The [Gang Roster](https://gitlab.com/r4dh4l/runcgr).
    3. Optional: Consider to use the [Gang Cashflow sheet](https://gitlab.com/r4dh4l/runcgr/-/blob/main/runcgr_cashflow.pdf) to protocol your credit flow (this will be some more effort in the beginning but will save you a lot of time during the campaign later calcualting the exact Gang Wealth value).
5. Read the [417 Dominion Campaign Reference](https://gitlab.com/r4dh4l/417dominion/-/blob/main/campaign_reference.pdf) to get a first impression how the Pre- and Post-Battle Sequence of this campaign work.

### For any match you play

Follow the steps of the [417 Dominion Campaign Reference](https://gitlab.com/r4dh4l/417dominion/-/blob/main/campaign_reference.pdf). A lot of them you can do **remotely in advance** (schedule match, maybe changing the scenario etc.). Keep in mind that the actual battle is just step 2 ("Battle Sequence") of overall 3 major steps, while the third step is the very important "Post-Battle Sequence" which needs at least as much time as a Battle Sequence.

# Campaign progress

## Territory overview

| Territory                     | Occupied by | Territory Boon | Enhanced Boon |
| ---                           | ---         | ---            | ---           |
| Old Ruins (p99)               | -           | Income: 1d3x10 credits, +1 to each d3 result for each Dome Runner in your Gang |
| Fighting Pit (Goliath) (p104) | pf          | 2 Hive Scum Hired Gungs for free incl. equipment prior to every battle | Goliath: Reputation: +2 |
| Corpse Farm (Cawdor) (p101))  | -           | Income: 1d6x10 credits per fighter removed from Rosters during preceding post-battle sequence  | Cawdor: Reputation: +1, Income: Territory Boon but with 2d6 |
| Tunnels (Orlock) (p105)       | -           | Special: Tunnel deployment for 3 fighters | Orlock: Repuration: +1, Special: Territory Boon but with 6 fighters |

Standard Territory Boons are replaced by Enhanced Boon of the same Boon type if available.

## Overall rankings

(in phase 1 = [P1 overall rankings](https://gitlab.com/r4dh4l/417dominion#p1-overall-rankings))

## P(hase) 1

### P1 overall rankings

**Dominator:**

| Gang ID | Territories occupied |
| ---     | ---                  |
| pf      | 1   |
| AmBot   | 0   |
| DSB     | 0   |
| P14     | 0   |
| nh      | 0   |
| NPFTD   | 0   |

**Slaughterer:**

| Gang ID | Enemy fighters taken (directly) Out of Action |
| ---     | ---                                           |
| pf      | 2   |
| AmBot   | 2   |
| DSB     | 0   |
| P14     | 0   |
| nh      | 0   |
| NPFTD   | 0   |

**Creditor:**

| Gang ID | Wealth |
| ---     | ---    |
| AmBot   | (phase end value not known yet) |
| pf      | (phase end value not known yet) |
| DSB     | (phase end value not known yet) |
| P14     | (phase end value not known yet) |
| nh      | (phase end value not known yet) |
| NPFTD   | (phase end value not known yet) |

**Warmonger:**

| Gang ID | Battles fought |
| ---     | ---            |
| pf      | 2   |
| P14     | 1   |
| AmBot   | 1   |
| DSB     | 0   |
| nh      | 0   |
| NPFTD   | 0   |


**Powerbroker:**

| Gang ID | Reputation |
| ---     | ---        |
| pf      | 17         |
| DSB     | 0          |
| P14     | 8          |
| AmBot   | 2          |
| nh      | 0          |
| NPFTD   | 0          |


**Arena Slaughterer (Gang):**

| Gang ID | Arena fighters taken (directly) Out of Action by the whole Gang |
| ---     | ---                                                             |
| AmBot   | 0   |
| DSB     | 0   |
| P14     | 0   |
| nh      | 0   |
| NPFTD   | 0   |
| pf      | 0   |

**Arena Slaughterer (Fighter):**

| Fighter name | Fighter ID | Arena fighters taken (directly) Out of Action by Fighter |
| ---          | ---        | ---                                                      |

### P1 matches

Matches are sorted chronologically earlier meeting/up to later meeting/down (the Match IDs of T(erritory) matches were provided at the begin of the phase which means that a higher T(erritory) Match ID can be listed before a lower because the according players met earlier for their match):

| Match ID | Gangs/Fighters involved (Gang Rating¹/Total Cost) | Winner | Territory                    | Scenario                | Att | Def   | Underdog |
| ---      | ---                                               | ---    | ---                          | ---                     | --- | ---   | ---      |
| P1T2     | **pf** (500) vs **AmBot** (500)                   | pf     | Fighting Pit (Goliath, p104) | Sneak Attack (p142)     | pf  | AmBot | -        |
| P1N1     | **pf** (500) vs **P14** (500)                     | ???    | -                            | Shootout (p166)         | -   | -     | -        |
| P1A1     | ***pf?*** (25???) vs ***P14?*** (192???)          | ???    | -                            | 417-1: Arena Fight      | -   | -     | -        |
| P1N2     | **pf** (???) vs **DSB** (???)                     | ???    | -                            | Sneak Attack (p142)     | ??? | ???   | -        |
| P1T1     | **DSB** (500?) vs **P14** (500?)                  | ???    | Old Ruins (p99)              | Sabotage (p134)         | -   | -     | -        |
| P1T3     | **NPFTD** (500?) vs **MILK** (500?)               | ???    | Corpse Farm (Cawdor, p101)   | Sneak Attack (p142)     | -   | -     | -        |
| P1T4     | **nh** (500?) vs **DSB** (500?)                   | ???    | Tunnels (Orlock) (p105)      | Looters (p126)          | -   | -     | -        |

¹the value as in your Pre-Battle Sequence step "Check Gang Ratings" (not later, not earlier)

Scenario pool for additional Non-Territory Matches:

- P1N3: Escort Mission (p174)
- P1N4: Caravan heist (p170)
- P1N5: The Hit (p182)

(if you want to change the scenario choose a scenario listed under "arbitrator tools", p160-180)

Match details from newest/up to oldest/down:

#### P1N2 details

    * Winner: pf
        * Territory won: (Non-Territory Fight)
    * Opponent Fighters taken (directly) Out of Action:
        * pf: 3
        * DSB: 1
        * New Total gang count (N/T / Arena):
            * pf: 5 / 1???
            * DSB: 1 / 0
    * Rewards:
        * Credits (scneario + territory + skill  = sum):
            * pf: 30 + 0 + 30 = 60
            * DSB: 30 + 0 + 0 = 30
        * ArcheoTech:
            * pf: 3
            * DSB: 0
        * Reputation +/- (scenario + arbitrator = sum):
            * pf: 1 + 6 = 7
            * DSB: 1 + 1 = 2
            * New Total Reputation:
                * pf: 24
                * DSB: 3
        * XP
            * pf
                * scenario: +1 per ganger (=3), +3 for winning
                * general: ???
            * DSB:
                * scenario: +1 per ganger (=4)
                * general: +5??? (=all opponents taken Out of Actoin?)
    * Fighters captived (Fighter ID):
        * by pf: -
        * by DSB: pf06

#### P1A1 details

    * Winner: P14
        * Territory won: (Non-Territory Fight)
    * Opponent Fighters taken (directly) Out of Action:
        * pf: 1
        * P14: 0
        * New Total gang count (N/T / Arena):
            * pf: 2 / 1
            * P14: 2 / 0
    * Rewards:
        * Credits (scneario + territory + skills  = sum):
            * pf: 0 + 0 + 50 = 50
            * P14: 56 + 0 + 0 = 56
        * ArcheoTech:
            * pf: 0
            * P14: 0
        * Reputation +/- (scenario + arbitrator = sum):
            * pf: 0 + -2 = -2 (=1)
            * P14: 2 + 3 = 5
            * New Total Reputation:
                * pf: 16
                * P14: 13
        * XP
            * pf
                * scenario: 4 (Underdog)
                * general: 0
            * P14:
                * scenario: 0
                * general: 1
    * Fighters captived (Fighter ID): (Arena Fight)
    
    
#### P1N1 details

    * Winner: pf
        * Territory won: (Non-Territory Fight)
    * Opponent Fighters taken (directly) Out of Action:
        * pf: 0
        * P14: 2
        * New Total gang count (N/T / Arena):
            * pf: 2 / 0
            * P14: 2 / 0
    * Rewards:
        * Credits ([scenario] + [territory] + [skills]  = MatchRewardSum):
            * pf: [35] + [0] + [40] = 75
            * P14: [0] + [0] + [0] = 0
        * ArcheoTech:
            * pf: ???
            * P14: ???
        * Reputation +/- (scenario + arbitrator = MatchRewardSum):
            * pf: 1 + 6 = 7
            * P14: 2 + 6 = 8
            * New Total Reputation:
                * pf: 17
                * P14: 8
        * eXPerience:
            * pf
                * scenario: +1 per ganger (=3)
                * general: +3
            * P14:
                * scneario: +1 per ganger (=2)
                * general: +2
    * Fighters captived (Fighter ID):
        * by pf: -
        * by P14: -

#### P1T2 details

    * Winner: pf
        * Territory won: Fighting Pit (Goliath, p104)
    * Opponent Fighters taken (directly) Out of Action:
        * pf: 2
        * AmBot: 2
        * New Total gang count (N/T / Arena):
            * pf: 2 / 0
            * AmBot: 2 / 0
    * Rewards:
        * Credits ([scenario] + [territory] + [skills] = MatchRewardSum):
            * pf:	[50] + [0] + [10+10]	=70
            * AmBot:[20] + [0] + [0] 		=20
        * ArcheoTech:
            * pf: 	1
            * AmBot:2
        * Reputation +/- ([scenario] + [arbitrator] = MatchRewardSum):
            * pf: 	[+1+2] + [+1+1+1+1+1]	= 8
            * AmBot:[+1-2] + [2] 			= 2
            * New Total Reputation:
                * pf: 	9
                * AmBot:2
        * eXPerience:
            * pf
                * scenario:	+1 per ganger (=5), +1 for leader
                * general: 	+2 for OoA events, +1 for a rallied ganger
            * AmBot:
                * scenario:	+1 per ganger (=???), +1 for leader
                * general: 	+1 for OoA event (second fighter OoA was a bonus fighter)
    * Fighters captived (Fighter ID):
        * by pf: 	-
        * by AmBot:	-

# FAQ and Errata

## Can I start over with a new Gang?

Changing your registered Gang is allowed any time but this means a complete startover for you (any progress you achieved is lost, all your territories become unoccupied).

## Does Territories grant Boons for Non-Territory Fights as well?

Yes (because a Non-Territory Fight can cause your gang casualties in the same way as a Territory Fights).

## How does a weapon with the Versatile trait effect opponents behind cover?

If the opponent behind covers goes prone and is not visible anymore, the opponent can not be attacked anymore (neither by general attacks using a Versatile weapon nor with Coup de Grace using a Versatile weapon).

## How does bonus fighters are treated related to smaller crew sizes?

Was discussed in phase 1 and the majority voted for "let's look how it works with standard rules". So nothing is changed. Tactics and Boons generate the amount if bonus fighters that are listed

## How should Bottle Tests with smaller crew sizes be handled?

This is up to you, see step "® © Decide the Bottle Test method" of the Pre-Battle Sequence in the Campaign Reference.

## Redistributing weapons amongst gang fighters

According to the offical rules Gangers would never ever give away their weapons (only wargear) because they have a strong bindings to this kind of "Underhive life policy". However, sometimes players would like to redistribute weapons anyway so in this campagin weapon redistribution is allowed in the way described by step "Distribute Equipment" in the Post-Battle Sequence of the [417 Dominion Campaign Reference](https://gitlab.com/r4dh4l/417dominion/-/blob/main/campaign_reference.pdf).

Example: I want to redistribute a Special Weapon of one of my Champions to one of my Ganger because the Ganger got a better Ballistics Skill during the course of the campain. I can not just take away the weapon from my Champion and give it to the Ganger. I have to "mediate" between the proud Champion as current weapon owner and the Ganger I want to benefit. I roll `1d` and get a `3` as result:
* Because the current weapon owner is a Champion the roll result is modified by `-1` to `2` ("Okay, you are still my Champion!").
* The benefited Ganger has more Arena Fighters taken OoA than the Champion so the result is modified by `+1` to `3` ("But you maybe shouldn't refuse this request in this case...").
* Because the Champion is offered a weapon in exchange (either from the inquiring Ganger or from the Gang Stash) the result is modified by `+1` to `4` ("...and you will get another nice weapon in exchange!").
* Unfortunately the offered Weapon is not from the same Weapon Category (in this case: Not a Special Weapon) so the result is modified by `-1` to 3. Because there are no other eligible modificators the negotation fails and the Special Weapon can not be redistributed from the Champion to the Ganger ("Well oaky, this offer was not fair at all.".

# Templates

## Match result submission

Replace:
* `<MatchID>` with the according Match ID (see match tables above of generate a free one)
* `<Gang ID1>` with the ID of the gangs participated (see participants tables above)

```
| <MatchID> | **<GangID1>** (<GangRatingOfGangID1>) vs **<GangID2>** (<GangRatingOfGangID2>) | <GangIDofWinner> | <TerritoryNameInCaseOfTerritoryFight> | Sneak Attack (p142) | <GangIDofAttacker> | <GangIDofDefender> | <GangIFofUnderdog> |
```

```
* **<MatchID> details**:
    * Winner: 
        * Territory won: 
    * Opponent Fighters taken (directly) Out of Action:
        * <Gang ID1>: ???
        * <Gang ID2>: ???
        * New Total gang count (N/T / Arena):
            * <Gang ID1>: ??? / ???
            * <Gang ID2>: ??? / ???
    * Rewards:
        * Credits ([scenario] + [territory] + [skills] = MatchRewardSum):
            * <Gang ID1>: ??? + ??? + ??? = ???
            * <Gang ID2>: ??? + ??? + ??? = ???
        * ArcheoTech:
            * <Gang ID1>: ???
            * <Gang ID2>: ???
        * Reputation +/- (scenario + arbitrator = MatchRewardSum):
            * <Gang ID1>: ??? + ??? = ???
            * <Gang ID2>: ??? + ??? = ???
            * New Total Reputation:
                * <Gang ID1>: ???
                * <Gang ID2>: ???
        * eXPerience:
            * <Gang ID1>
                * scenario: ???
                * general: ???
            * <Gang ID2>:
                * scenario: ???
                * general: ???
    * Fighters captived (Fighter ID):
        * by <Gang ID1>: ???
        * by <Gang ID2>: ???
```

Only when the **end of a campaign phase** was announced: 

Replace:
* `<Gang ID>` with the ID of your gang

```
* **<GangID>**:
    * Gang Rating:
    * Reputation:
    * Total Fighters taken Out of Action (N/T / Arena):
            * (Non-)Territory matches: ???
            * Arena matches: ???
    * Wealth (Gang Ranking + Stash stuff in credits): ???
```


### Example for a match result submission

| Match ID | Gangs involved (Gang Rating¹)   | Winner | Territory                    | Scenario                | Att | Def   | Underdog |
| ---      | ---                             | ---    | ---                          | ---                     | --- | ---   | ---      |
| P1T2     | **pf** (500) vs **AmBot** (500) | pf     | Fighting Pit (Goliath, p104) | Sneak Attack (p142)     | pf  | AmBot | -        |

¹the value as in your Pre-Battle Sequence step "Check Gang Ratings" (not later, not earlier)

* **P1T2 details**:
    * Winner: pf
        * Territory won: Fighting Pit (Goliath, p104)
    * Opponent Fighters taken (directly) Out of Action:
        * pf: 2 [means: *During the match* the gang "pf" took 2 opponent fighters Out of Action – what happens later to these fighters (if they survive or die) is irrevelant and "Out of Action" does not mean "kill"]
        * AmBot: 2
        * New Total gang count (N/T / Arena):
            * pf: 2 / 0 [means: The total count of fighters the gang "pf" took Out of Action in *all* (**N**on-)**T**erritory Fights is `2` and in all Arena Fights is `0`]
            * AmBot: 2 / 0
    * Rewards:
        * Credits ([scenario] + [territory] + [skills] = MatchRewardSum):
            * pf:	[50] + [0] + [10+10]	=70
            * AmBot:[20] + [0] + [0] 		=20
        * ArcheoTech:
            * pf: 	1
            * AmBot:2
        * Reputation +/- ([scenario] + [arbitrator] = MatchRewardSum):
            * pf: 	[+1+2] + [+1+1+1+1+1]	= 8
            * AmBot:[+1-2] + [2] 			= 2
            * New Total Reputation:
                * pf: 	9
                * AmBot:2
        * eXPerience:
            * pf
                * scenario:	+1 per ganger (=5), +1 for leader [means: each fighter of the Gang "pf" got 1 XP and the leader 2 XP according to general rules)
                * general: 	+2 for OoA events, +1 for a rallied ganger [means: some fighter(s) fot 2XP for taking fighters Out of Action and a certain fighter got 1XP a successful rally after being Broken)
            * AmBot:
                * scenario:	+1 per ganger (=5), +1 for leader
                * general: 	+1 for OoA event (second fighter OoA was a bonus fighter)
    * Fighters captived (Fighter ID):
        * by pf: 	AmBot04 [means: Gang "pf" got the fighter with the ID `AmBot04` as captive)
        * by AmBot:	pf03
